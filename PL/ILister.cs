﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PL
{
    public delegate T EditAction<T>(T item, int index, List<T> list);
    public delegate T AddAction<T>(int index, List<T> list);
    public delegate void RemoveAction<T>(T item, int index, List<T> list);
    public delegate void MoreInfoAction<T>(T item, int index, List<T> list);

    public interface ILister<T>
    {
        List<T> getList();
        void update();
        void setDataList(List<T> list);
        void setEditAction(EditAction<T> action);
        void setMoreInfoAction(EditAction<T> action);
        void setRemoveAction(RemoveAction<T> action);
    }
}
