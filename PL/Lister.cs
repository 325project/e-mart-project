﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace PL
{
    class Lister<T>
    {
        List<T> list;
        List<FieldInfo> fields;
        Dictionary<string, int> columnSizes;
        const string VERTICAL_DIVIDOR = "|";
        const char HORIZONTAL_DIVIDOR = '~';
        int tableSize;
        private int cursor = 0;
        private int shift = 0;
        private const int CONSOLE_WIDTH = 80;

        public delegate void RemoveAction(T item, int index, List<T> list);
        public delegate T EditAction(T item, int index, List<T> list);
        public delegate void MoreInfo(T item, int index, List<T> list);
        public delegate T AddAction(T item, int index, List<T> list);

        RemoveAction ra;
        EditAction ea;
        MoreInfo mi;
        AddAction aa;

        public void setEditAction(EditAction action)
        {
            ea = action;
        }

        public void setRemoveAction(RemoveAction action)
        {
            ra = action;
        }

        public void setMoreInfo(MoreInfo action)
        {
            mi = action;
        }

        public void setAddAction(AddAction action)
        {
            aa = action;
        }

        public void setDataList(List<T> list)
        {
            this.list = list;
            update();
        }

        public Lister()
        {
            list = new List<T>();
            init();
        }

        public Lister(List<T> list)
        {
            this.list = list;
            init();
        }

        private void init()
        {
            printTable();
        }

        public void update()
        {
            fields = typeof(T).GetFields().ToList();
            columnSizes = measureColumns();
            tableSize = 0;
            for (int i = 0; i < columnSizes.Count(); i++)
                tableSize += columnSizes.ElementAt(i).Value + 1;

            slide(draw(), shift);
        }

        public string draw()
        {
            string buff = "";
            int k = 0;
            int[] max = new int[fields.Count];
            foreach (FieldInfo f in fields)
            {
                max[k] = maxInfoLength(f);
                tableSize += Math.Max(max[k] - f.Name.Length, 0);
                k++;
            }
            k = 0;

            tableSize += fields.Count;

            foreach (FieldInfo f in fields)
            {
                buff += f.Name.PadRight(columnSizes[f.Name]).PadRight(max[k]) + " " + VERTICAL_DIVIDOR;
                k++;
            }

            buff += '\n';

            for (int i = 0; i < tableSize; i++)
                buff += HORIZONTAL_DIVIDOR;
            buff += '\n';
            foreach (T item in list)
            {
                k = 0;
                foreach (FieldInfo f in fields)
                {
                    string val = f.GetValue(item) == null ? "null" : f.GetValue(item).ToString();
                    buff += val.ToString().PadRight(max[k]) + " " + VERTICAL_DIVIDOR;
                    k++;
                }

                buff += '\n';
            }

            return buff;
        }

        private int maxInfoLength(FieldInfo f)
        {
            int max = 0;
            for (int i = 0; i < list.Count; i++)
            {
                if (f.GetValue(list[i]) != null)
                {
                    max = Math.Max(max, f.GetValue(list[i]).ToString().Length);
                }
                max = Math.Max(max, f.Name.Length);
            }
            return max;
        }

        private void slide(string s, int shift)
        {
            int i = 0;
            foreach (string line in s.Split('\n'))
            {
                Console.BackgroundColor = (i == cursor + 2) ? ConsoleColor.White : ConsoleColor.Black;
                Console.ForegroundColor = (i == cursor + 2) ? ConsoleColor.Black : ConsoleColor.White;
                string val = (line.Count() <= CONSOLE_WIDTH) ? line : line.Substring(shift, CONSOLE_WIDTH);
                if (line.Count() <= CONSOLE_WIDTH)
                    Console.WriteLine(val);
                else
                    Console.Write(val);
                i++;
            }
        }

        private Dictionary<string, int> measureColumns()
        {
            Dictionary<string, int> sizes = new Dictionary<string, int>();

            foreach (FieldInfo f in fields)
            {
                if (!sizes.ContainsKey(f.Name))
                    sizes.Add(f.Name, 0);
                sizes[f.Name] = Math.Max(sizes[f.Name], f.Name.Count());
            }


            foreach (T item in list)
                foreach (FieldInfo f in fields)
                {
                    string val = f.GetValue(item) == null ? "null" : f.GetValue(item).ToString();
                    sizes[f.Name] = Math.Max(val.Count(), f.Name.Count());
                }


            return sizes;
        }

        public List<T> getList()
        {
            return list;
        }

        public void printTable()
        {
            while (true)
            {
                Console.Clear();
                update();
                var key = Console.ReadKey().Key;

                switch (key)
                {
                    case ConsoleKey.LeftArrow:
                        shift = Math.Max(0, --shift);
                        break;

                    case ConsoleKey.RightArrow:
                        shift = Math.Min(Math.Max(0, tableSize - CONSOLE_WIDTH), ++shift);
                        break;

                    case ConsoleKey.UpArrow:
                        cursor = Math.Max(0, --cursor);
                        break;

                    case ConsoleKey.DownArrow:
                        cursor = Math.Min(list.Count() - 1, ++cursor);
                        break;

                    case ConsoleKey.E:
                        {
                            if (ea != null)
                            {
                                if (list.Count() == 0)
                                {
                                    // TODO: add an error for not having what to edit
                                    Console.ReadLine();
                                }
                                else
                                {
                                    list[cursor] = ea(list[cursor], cursor, list);
                                    update();
                                }
                            }
                        }
                        break;

                    case ConsoleKey.R:
                        {
                            if (ra != null)
                            {
                                if (list.Count() == 0)
                                {
                                    // TODO: add an error for not having what to remove
                                    Console.ReadLine();
                                }
                                else
                                {
                                    ra(list[cursor], cursor, list);
                                    update();
                                }
                            }
                        }
                        break;

                    case ConsoleKey.M:
                        {
                            if (mi != null)
                            {
                                if (list.Count() == 0)
                                {
                                    // TODO: add an error for not having what to show
                                    Console.ReadLine();
                                }
                                else
                                {
                                    mi(list[cursor], cursor, list);
                                    update();
                                }
                            }
                        }
                        break;

                    case ConsoleKey.A:
                        {
                            if (aa != null)
                            {
                                list.Add(aa(list[cursor], cursor, list));
                                update();
                            }
                        }
                        break;

                    case ConsoleKey.Escape:
                        {
                            cursor = Math.Min(list.Count() - 1, ++cursor);
                            draw();
                            return;
                        }
                }
            }
        }
    }
}
