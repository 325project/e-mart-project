﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BL;
using Common;
using PL.Menus.QueryMenus;

namespace PL.Menus
{
    class QuerySelectorMenu : Menu
    {
        IBL bl;
        public QuerySelectorMenu(IBL bl) 
        {
            this.bl = bl;
        }

        override
        public Dictionary<string, MenuAction> getMenu()
        {
            Dictionary<string, MenuAction> menu = new Dictionary<string, MenuAction>();
            menu.Add("query employees", queryEmployeesAction);
            menu.Add("query products", queryProductsAction);
            menu.Add("query club members", queryClubMembersAction);
            menu.Add("query departments", queryDepartmentsAction);
            return menu;
        }

        void queryEmployeesAction()
        {
            new EmployeeQueryMenu(bl).start();
        }

        void queryProductsAction()
        {
            new ProductQueryMenu(bl).start();
        }

        void queryClubMembersAction()
        {
            new ClubMemberQueryMenu(bl).start();
        }

        void queryDepartmentsAction()
        {
            new DepartmentQueryMenu(bl).start();
        }
    }
}
