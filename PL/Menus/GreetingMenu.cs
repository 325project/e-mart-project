﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using BL;

namespace PL.Menus
{
    class GreetingMenu : Menu
    {
        IBL bl;
        public GreetingMenu(IBL bl) 
        {
            this.bl = bl;
        }

        override
        public Dictionary<string, MenuAction> getMenu() 
        {
            Dictionary<string, MenuAction> menu = new Dictionary<string, MenuAction>();
            menu.Add("login as supervisor", supervisorLoginAction);
            menu.Add("signup as supervisor", supervisorSignupAction);
            menu.Add("login as employee", employeeLoginAction);
            menu.Add("signup as employee", employeeSignupAction);
            return menu;
        }

        void supervisorLoginAction()
        {
            Console.WriteLine("username: ");
            string username = Console.ReadLine();
            Console.WriteLine("password: ");
            string password = Console.ReadLine();

            Supervisor s = bl.supervisorLogin(username, password);
            if (s == null)
            {
                Console.WriteLine("supervisor not found !");
                Console.WriteLine("press any key to continue ...");
                Console.ReadKey();
                start();
            }
                    
            else 
            {
                Console.WriteLine("logged in !");
                Console.WriteLine("press any key to continue ...");
                Console.ReadKey();
                new MainMenu(bl).start();
            }
            
        }

        void employeeLoginAction()
        {
            Console.WriteLine("username: ");
            string username = Console.ReadLine();
            Console.WriteLine("password: ");
            string password = Console.ReadLine();

            Employee e = bl.employeeLogin(username, password);
            if (e == null)
            {
                Console.WriteLine("employee not found !");
                Console.WriteLine("press any key to continue ...");
                Console.ReadKey();
                start();
            }
            else
            {
                Console.WriteLine("logged in !");
                Console.WriteLine("press any key to continue ...");
                Console.ReadKey();
                new MainMenu(bl).start();
            }
            
        }

        void supervisorSignupAction()
        {
            Supervisor e = new Editor<Supervisor>().start();
            try
            {
                if(e != null)
                    bl.supervisorSignup(e);
                new GreetingMenu(bl).start();
            }
            catch (Exception err)
            {
                Console.WriteLine(err.Message);
                Console.WriteLine("press any key to continue ...");
                Console.ReadKey();
                start();
            }
            
        }

        void employeeSignupAction()
        {
            Employee e = new Editor<Employee>().start();
            try
            {
                if (e != null)
                    bl.employeeSignup(e);
                new GreetingMenu(bl).start();
            }
            catch (Exception err)
            {
                Console.WriteLine(err.Message);
                Console.WriteLine("press any key to continue ...");
                Console.ReadKey();
                start();
            }
        }
    }
}
