﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common;

namespace BL
{
    public interface IBL
    {
        #region session management
        User getCurrentUser();

        Employee employeeLogin(string username, string password);
        Employee employeeByID(string id);
        Employee employeeSignup(Employee u);
        List<Employee> allEmployees();

        Supervisor supervisorLogin(string username, string password);
        Supervisor supervisorById(string ID);
        Supervisor supervisorSignup(Supervisor u);
        #endregion

        #region manage data

        Product addProduct(Product p);
        Product removeProduct(string id);
        Product updateProduct(Product p);
        List<Product> listProducts();

        ClubMember addClubMember(ClubMember c);
        ClubMember removeClubMember(string id);
        ClubMember updateClubMember(ClubMember c);
        List<ClubMember> listClubMembers();

        Department addDepartment(Department p);
        Department removeDepartment(string id);
        Department updateDepartment(Department p);
        List<Department> listDepartments();

        #endregion

        #region manage employees

        Employee addEmployee(Employee p);
        Employee removeEmployee(string id);
        Employee updateEmployee(Employee p);
        List<Employee> listEmployees();

        #endregion

        #region queries

        List<Employee> employeeByName(string fname, string lname);
        List<Employee> employeeBySupervisorID(string sID);
        List<Employee> employeeByDepartmentID(string dID);
        List<Employee> employeeBySalaryRange(float min, float max);

        Product productByID(string id);
        List<Product> productByName(string name);
        List<Product> productByType(Product.Type type);
        List<Product> productByDepartmentID(string dID);
        List<Product> productByStockCount(int min , int max);
        List<Product> productByDateRange(DateTime min, DateTime max);

        ClubMember clubmemberByID(string id);
        List<ClubMember> clubmemberByName(string fname, string lname);
        List<ClubMember> clubmemberByDateRange(DateTime min, DateTime max);

        Department departmentByID(string id);
        List<Department> departmentByName(string name);

        #endregion
    }
}
