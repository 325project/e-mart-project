﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    [Serializable]
    public class Product
    {
        public enum Type { Food , Furniture }

        [Required]
        [Display(Name = "ID")]
        public string ID;

        [Required]
        [Display(Name = "name")]
        public string name;

        [Required]
        [Display(Name = "type")]
        public Type type;

        [Required]
        [Display(Name = "department ID")]
        public string departmentID;

        [Display(Name = "is in stock")]
        public bool isInStock;

        [Display(Name = "stock count")]
        public int stockCount;
    }
}
