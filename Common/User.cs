﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Common
{
    [Serializable]
    public class User : Person
    {
        [Required]
        [Display(Name = "username")]
        public string username;

        [Required]
        [Display(Name = "password")]
        public string password;

        public bool isCredsCorrect(string u , string p) 
        {
            return username == u && password == p;
        }
    }
}
